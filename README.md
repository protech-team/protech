# Protech

Ce projet a été generé avec [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

## Serveur de Development

Pour lancer le serveur de developpement executez `ng serve`. Naviguez vers `http://localhost:4200/`. Le hot reload permet au server de se relancer a chaque modification.

## Code scaffolding

Executez `ng generate component component-name` pour generer un nouveau composent. Vous pouvez toutefois utiliser `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

La commande `ng build` vous permet de compiler le project(au format conventionel pour les deploiements web). Le build generé sera stocké dans le repertoire `dist/`. Utilisez l'option `--prod` pour generer un build pour la production( directement deployable sur un hebergeur).

## tests unitaires

Les test sont lancés par la commande `ng test` via [Karma](https://karma-runner.github.io).

## Les tests end-to-end

Pour cela utilisez `ng e2e` via [Protractor](http://www.protractortest.org/).

## Plus d'Aide

Plus d'informations sur Angukar CLI ici `ng help` ou sur [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

Redigé par HLabs