import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { IUser, ICategorie, FirebaseService } from '../services/firebase/firebase.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  users: IUser[] = [];
  categories: ICategorie[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public firebaseService: FirebaseService
  ) { }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories(): void {
    this.firebaseService.getCategories().subscribe((res) => {
      this.categories = res.map((p) => {
        return {
          id: p.payload.doc.id,
          ...(p.payload.doc.data() as ICategorie),
        } as ICategorie;
      });
      console.log(this.categories);
    });
  }

}
