import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase';
import { IProjet, IUser, FirebaseService } from '../services/firebase/firebase.service';

@Component({
  selector: 'app-description-projet',
  templateUrl: './description-projet.component.html',
  styleUrls: ['./description-projet.component.css']
})
export class DescriptionProjetComponent implements OnInit {
  projet: IProjet[];
  ids;
  currentUsers: IUser[] = [];
  postuleDeja: boolean;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    public firebaseService: FirebaseService
  ) { }

  ngOnInit(): void {
    this.postuleDeja = false;
    this.getCurrentProjet();
  }

  getCurrentProjet(): void {
    this.ids = this.route.snapshot.paramMap.get('id');
    this.firebaseService.getProjets().subscribe((res) => {
      this.projet = res.map((p) => {
        return {
          id: p.payload.doc.id,
          ...(p.payload.doc.data() as IProjet),
        } as IProjet;
      });
      console.log(this.projet);
    });
  }

  // tslint:disable: typedef
  postuler(id) {
    firebase
      .firestore()
      .collection('user')
      .where('email', '==', firebase.auth().currentUser.email)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.currentUsers.push({
              id: doc.id,
              ...(doc.data() as IUser),
            });
          });
          this.firebaseService.addCandidature({
            candidatId: this.currentUsers[0].id,
            projetId: id,
          });
          this.postuleDeja = true;
          alert('Votre candidature a bien été prise en compte');
        },
        () => { }
      );
  }


}
