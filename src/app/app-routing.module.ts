import { DescriptionProjetConsultantComponent } from './description-projet-consultant/description-projet-consultant.component';
import { AutresProjetsComponent } from './autres-projets/autres-projets.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { OffresComponent } from './offres/offres.component';
import { VueProjetEnterpriseComponent } from './vue-projet-enterprise/vue-projet-enterprise.component';
import { MesProjetsComponent } from './mes-projets/mes-projets.component';
import { LayoutEnterpriseComponent } from './layout-enterprise/layout-enterprise.component';
import { DescriptionProjetComponent } from './description-projet/description-projet.component';
import { LoginComponent } from './login/login.component';
import { VueProjetComponent } from './vue-projet/vue-projet.component';
import { HomeConsultantComponent } from './home-consultant/home-consultant.component';
import { HomeComponent } from './home/home.component';
import { ListeProjetsComponent } from './liste-projets/liste-projets.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutAuthComponent } from './layout-auth/layout-auth.component';
import { LayoutConsultantComponent } from './layout-consultant/layout-consultant.component';
import { SignUpEnterpriseComponent } from './sign-up-enterprise/sign-up-enterprise.component';
import { AuthGuardComponent } from './services/auth-guard/auth-guard.component';
import { LayoutAnonymeComponent } from './layout-anonyme/layout-anonyme.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutAnonymeComponent,
    children: [
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'list-projet/:libelle/:id',
        component: ListeProjetsComponent,
      },
      {
        path: 'description-projet/:id',
        component: DescriptionProjetComponent,
      },
    ],
  },
  // routes consultant debut
  {
    path: 'consultant',
    canActivate: [AuthGuardComponent],
    component: LayoutConsultantComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'projets',
        component: ListeProjetsComponent,
      },
      {
        path: 'mes-projets',
        component: MesProjetsComponent,
      },
      {
        path: 'home',
        component: HomeConsultantComponent,
      },
      {
        path: 'vue-projet/:id',
        component: VueProjetComponent,
      },
      {
        path: 'autres-projets',
        component: AutresProjetsComponent,
      },
      {
        path: 'description-projet-consultant/:id',
        component: DescriptionProjetConsultantComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
    ],
  },
  // routes consultant end

  // routes entreprise debut
  {
    path: 'entreprise',
    component: LayoutEnterpriseComponent,
    canActivate: [AuthGuardComponent],
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        component: MesProjetsComponent,
      },
      {
        path: 'mes-projets',
        component: MesProjetsComponent,
      },
      {
        path: 'vue-projet/:id',
        component: VueProjetComponent,
        // component: VueProjetEnterpriseComponent,
      },
      {
        path: 'offres',
        component: OffresComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
    ],
  },
  // routes entreprise end

  // routes authentification debut
  {
    path: 'auth/login',
    component: LoginComponent,
  },
  {
    path: 'auth/signup-const',
    component: SignUpComponent,
  },
  {
    path: 'auth/signup-ent',
    component: SignUpEnterpriseComponent,
  },
  // routes authentification end
  // {
  //   path: '**',
  //   redirectTo: '/home',
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
