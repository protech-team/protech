import { ITache } from './../services/firebase/firebase.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase';
import {
  IMembreProjet,
  ICandidature,
  FirebaseService,
  ICategorie,
  IProjet,
  IUser,
} from '../services/firebase/firebase.service';

@Component({
  selector: 'app-vue-projet',
  templateUrl: './vue-projet.component.html',
  styleUrls: ['./vue-projet.component.css'],
})
export class VueProjetComponent implements OnInit {
  ajouterTacheForm: FormGroup;
  errorMessage: string;
  projet: IProjet[];
  candidatures: ICandidature[] = [];
  membres: IMembreProjet[] = [];
  postulants: IUser[] = [];
  intervenants: IUser[] = [];
  taches: ITache[] = [];
  tachesValidees: ITache[] = [];
  bloquerBoutons: string[] = [];
  currentUsers: IUser[] = [];
  ids;
  myUser;
  taux;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    public firebaseService: FirebaseService
  ) {}

  ngOnInit(): void {
    this.getCurrentUsers();
    // this.getCurrentProjet();
    this.initForm();
  }

  initForm(): void {
    this.ajouterTacheForm = this.formBuilder.group({
      nom: ['', [Validators.required, Validators.email]],
      description: ['', [Validators.required]],
      dateLimite: ['', [Validators.required]],
      chargeTacheId: ['', [Validators.required]],
      phase: ['', [Validators.required]],
    });
  }

  onSubmit(): void {
    const data = {
      nom: this.ajouterTacheForm.get('nom').value,
      description: this.ajouterTacheForm.get('description').value,
      dateLimite: this.ajouterTacheForm.get('dateLimite').value,
      chargeTacheId: this.intervenants[
        this.ajouterTacheForm.get('chargeTacheId').value
      ].id,
      chargeTacheNom: this.intervenants[
        this.ajouterTacheForm.get('chargeTacheId').value
      ].nomComplet,
      chargeTacheAvatar: this.intervenants[
        this.ajouterTacheForm.get('chargeTacheId').value
      ].avatarUrl,
      phase: this.ajouterTacheForm.get('phase').value,
      statut: 'En attente',
      projetId: this.ids,
      validated: false,
    };

    this.addTache(data);

    alert('La tâche a bien été ajoutée');

    this.initForm();
  }

  addTache(data: ITache): void {
    this.firebaseService.addTache(data).then((res) => {
      res.get().then((r) => {
        this.taches.push({
          id: r.id,
          ...(r.data() as ITache),
        });
      });
    });
  }

  getCurrentProjet(): void {
    this.ids = this.route.snapshot.paramMap.get('id');
    this.firebaseService.getProjets().subscribe((res) => {
      this.projet = res.map((p) => {
        return {
          id: p.payload.doc.id,
          ...(p.payload.doc.data() as IProjet),
        } as IProjet;
      });
      console.log('Projet', this.projet);
    });

    firebase
      .firestore()
      .collection('candidature')
      .where('projetId', '==', this.ids)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.candidatures.push({
              id: doc.id,
              ...(doc.data() as ICandidature),
            } as ICandidature);
          });
          console.log('Candidatures', this.candidatures);

          this.candidatures.forEach((c) => {
            firebase
              .firestore()
              .collection('user')
              .doc(c.candidatId)
              .get()
              .then((r) => {
                this.postulants.push({
                  id: r.id,
                  ...(r.data() as IUser),
                });
                this.bloquerBoutons.push('false');
              });
          });
          console.log('Postulants', this.postulants);
        },
        () => {}
      );

    firebase
      .firestore()
      .collection('membre')
      .where('projetId', '==', this.ids)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.membres.push({
              id: doc.id,
              ...(doc.data() as IMembreProjet),
            } as IMembreProjet);
          });
          console.log('Membres projets : ', this.membres);

          this.membres.forEach((m) => {
            firebase
              .firestore()
              .collection('user')
              .doc(m.membreId)
              .get()
              .then((r) => {
                this.intervenants.push({
                  id: r.id,
                  ...(r.data() as IUser),
                });
              });
          });
          console.log('Intervenants ', this.intervenants);
        },
        () => {}
      );

    firebase
      .firestore()
      .collection('tache')
      .where('projetId', '==', this.ids)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.taches.push({
              id: doc.id,
              ...(doc.data() as ITache),
            } as ITache);
          });
          this.tachesValidees = this.taches.filter(this.filtrerValidees);
          this.taux = (this.tachesValidees.length / this.taches.length) * 100;
          console.log('taux : ', this.taux);
        },
        () => {}
      );
  }

  addPostulant(id: string, i: number) {
    console.log('id = ', id, ' i = ', i, ' projet.id = ', this.projet[i].id);
    this.firebaseService.addMembreProjet({
      membreId: this.postulants[i].id,
      projetId: id,
    });

    this.bloquerBoutons[i] = 'true';

    alert('Le postulant a bien été ajouté');
  }

  validerTache(index) {
    this.taches[index].validated = true;
    this.taches[index].statut = 'Terminée';
    this.firebaseService.updateTache(this.taches[index].id, this.taches[index]);
  }

  lancerTache(index) {
    this.taches[index].statut = 'En cours';
    this.firebaseService.updateTache(this.taches[index].id, this.taches[index]);
  }

  terminerTache(index) {
    this.taches[index].statut = 'Terminée';
    this.firebaseService.updateTache(this.taches[index].id, this.taches[index]);
  }

  terminerProjet(index) {
    this.projet[index].statut = 'Terminée';
    this.firebaseService.updateProjet(this.projet[index].id, this.projet[index]);
  }

  getCurrentUsers() {
    firebase
      .firestore()
      .collection('user')
      .where('email', '==', firebase.auth().currentUser.email)
      .get()
      .then(
        (res) => {
          res.forEach((doc) => {
            this.currentUsers.push({
              id: doc.id,
              ...(doc.data() as IUser),
            });
          });
          this.myUser = firebase.auth().currentUser.uid;
          this.getCurrentProjet();
        },
        () => {}
      );
  }

  filtrerValidees(abc) {
    return abc.validated === true;
  }
}
